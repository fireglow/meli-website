#!/bin/bash

# sed Removes trailing whitespace in <pre> elements
wget --timestamping "https://git.meli.delivery/meli/meli/raw/branch/master/"$1 -O - | mandoc -I os="rendered by mandoc" -Kutf-8 -Ofragment,toc,style="css/manpage.css",includes="#%I",man="#%N.%S" -Thtml | sed 's/\s*<\/pre/<\/pre/'
