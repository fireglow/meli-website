---
html_title: meli terminal mail client
showTitle: false
mainClass: wrapper
bees: What if bees could travel through paintings?
screenshots:
 - url : "/images/screenshots/conversations.webp"
   desc: "Threads take one row by default."
 - url : "/images/screenshots/conversations-view-open-thread.webp"
   desc: "Viewing a thread."
 - url : "/images/screenshots/threads.webp"
   desc: "Compact view."
 - url : "/images/screenshots/single-thread-view.webp"
   desc: "Viewing a single thread. (UI not final)"
 - url : "/images/screenshots/composing.svg"
   desc: "Composing replies"
tag: alpha-0.6.0
---
